

<!-- Header -->
<?php include './include/header.php' ?>
<!-- Header -->

        
<!-- Main Banner  -->

<div class="main-wraper contact-page">
       
    <!-- Menu -->
    <?php include './include/menu.php' ?>
    <!-- Menu -->

</div>
<!-- Main Banner  -->


<!-- Contact Content Area -->
<section class="contact-content-area">
    <div class="container">
        <div class="contact-content-inner">
            <h1 class="sec-heading">Contact us</h1>
            <h3>Email</h3>
            <p>
                <a href="mailto:giving@habib.edu.pk">giving@habib.edu.pk</a>
            </p>
            <h3>Call us at</h3>
            <p>
                <a href="tel:+92 21 111 042 242">+92 21 111 042 242</a>(Ext. 4444) | <a href="tel:+92 312 2338343"> +92 312 2338343</a>
            </p>
            <h3>Mailling Address</h3>
            <p>HUF, Block 18, Gulistan-e-Jauhar - University Avenue, Off Shahrah-e-Faisal, <br>
                Karachi - 75290, Sindh, Pakistan</p>
        </div>
    </div>
</section>
<!-- Contact Content Area -->

<!-- Footer -->
<?php include './include/footer.php' ?>
<!-- Footer -->

  