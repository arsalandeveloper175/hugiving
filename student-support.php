

<!-- Header -->
<?php include './include/header.php' ?>
<!-- Header -->




        
<!-- Main Banner  -->

<div class="main-wraper student-support">
     
    <!-- Menu -->
    <?php include './include/menu.php' ?>
    <!-- Menu -->

   <div class="student-sup-in">
       <div class="banner-content">
           <h1>Ways Of Giving</h1>
           <h2>Student Support</h2>
           <p>Habib University’s generosity is unparalleled, providing up to 90 percent students with scholarships and financial aid. You can choose to make your contribution by investing in the next generation of leaders.</p>
       </div>
   </div>

</div>
<!-- Main Banner  -->


<!-- Recurring Scholerships -->
<section class="recurring-scholership">
    <div class="container">
        <div class="section-heading-area">
            <h1>Recurring Scholarships</h1>
            <p>The recurring scholarship program allows you to make a contribution on an annual basis to help students attend Habib University.</p>
        </div>
    </div>
    <div class="container p-0">
        <div class="recuring-scholer-slider owl-theme owl-carousel">
            <div class="recurting-slider-item">
                <img src="img/student-support/item1.webp" alt="" class="img-fluid">
                <div class="recur-item-cont">
                    <h4>Talent Outreach Promotion Support Program</h4>
                    <p>The recurring scholarship program allows you to make a contribution on an annual basis to help students attend Habib University.</p>
                </div>
            </div>
            <div class="recurting-slider-item">
                <img src="img/student-support/item2.webp" alt="" class="img-fluid">
                <div class="recur-item-cont">
                    <h4>Yohsin Scholarship</h4>
                    <p>The Yohsin Scholarship is for students who embody Habib’s values of passion and excellence. It is a 100% scholarship for high-merit students.</p>
                </div>
            </div>
            <div class="recurting-slider-item">
                <img src="img/student-support/item3.webp" alt="" class="img-fluid">
                <div class="recur-item-cont">
                    <h4>Habib Excellence Scholarship</h4>
                    <p>The Habib Excellence Scholarship is available to students who demonstrate excellent merit, covering 60 to 80 percent of the tuition and additional fees of the recipient.</p>
                </div>
            </div>
            <div class="recurting-slider-item">
                <img src="img/student-support/item4.webp" alt="" class="img-fluid">
                <div class="recur-item-cont">
                    <h4>Habib Merit Scholarship</h4>
                    <p>The Habib Merit Scholarship is awarded to students who demonstrate high merit, and covers up to 50 percent of the tuition and additional fees of the recipient.</p>
                </div>
            </div>
            
            <div class="recurting-slider-item">
                <img src="img/student-support/item5.webp" alt="" class="img-fluid">
                <div class="recur-item-cont">
                    <h4>Transforming Higher Education with Your Zakat</h4>
                    <p>Donations to Habib University are 100 percent Zakat certified, and you can make your contribution today so that deserving students can obtain a world-class education.</p>
                </div>
            </div>
            <div class="recurting-slider-item">
                <img src="img/student-support/item6.webp" alt="" class="img-fluid">
                <div class="recur-item-cont">
                    <h4>General Financial Aid Program</h4>
                    <p>In addition to scholarships, students benefit from a generous financial aid program. With your help, students are able to focus on their studies with less pressure of fees.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Recurring Scholerships -->

<!-- Common Scholership area -->
<section class="common-shollership-area">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4 pr-0 p-sm-0 p-xs-0">
                <div class="scholer-images">
                    <img src="img/student-support/scholarship-image.webp" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-8 pl-0 p-sm-0 p-xs-0">
                <div class="scholer-conternt">
                   <h1> Endowed Scholarship</h1>
                   <p> You can also endow a scholarship in your name, ensuring students access the learning needed for a better future. The gift of an endowed scholarship enables Habib University to continue providing scholarship aid to students who are in need and deserving of assistance in pursuing their Big Dreams</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Common Scholership area -->

<!-- Make Gift -->
<?php include './include/make-gift.php' ?>
<!-- Make Gift -->




<!-- Footer -->
<?php include './include/footer.php' ?>
<!-- Footer -->

