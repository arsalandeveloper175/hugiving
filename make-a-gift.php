

<!-- Header -->
<?php include './include/header.php' ?>
<!-- Header -->




<!-- Main Banner  -->

<div class="main-wraper make-gift-main">
        
    <!-- Menu -->
    <?php include './include/menu.php' ?>
    <!-- Menu -->

   <div class="student-sup-in">
       <div class="banner-content">
           <h1>How To Make a Gift</h1>
           <!-- <h2>How To Make a Gift </h2> -->
       </div>
       <!-- <div class="container">
           <div class="row justify-content-center align-items-center">
               <div class="col-lg-10">
               </div>
           </div>
       </div> -->
   </div>

</div>
<!-- Main Banner  -->

<section class="secure-gift-online">
    <div class="container">
        <div class="secure-accordians">
            <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <div class="main-accor-title">
                            <span class="flg-acc"><img src="img/make-gift/pk.svg" alt="" class="img-fluid"></span>
                            <span class="title-acc">In Pakistan</span>
                        </div>
                        <i class="fas fa-chevron-down"></i>
                      </button>
                    </h2>
                  </div>
              
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                       <div class="secure-content-area">
                           <h3>Make a Secure Gift Online </h3>
                           <p>You can make small regular contribution to Habib and ensure access for the most talented and deserving students to a world class education. </p>
                           <a href="https://giving.habib.edu.pk/" class="give-now">Give Now</a>

                           <div class="secure-pay-details">
                               <div class="row">
                                   <div class="col-lg-6">
                                       <div class="secur-tran-detail">
                                           <h3><img src="img/make-gift/money-transfer.svg" alt=""> Online Transfer: </h3>
                                           <p>
                                               <span>Title: </span>Habib University Foundation
                                           </p>
                                           <p>
                                                <span>A/c Number: </span>06-01-01-20311-714-422193
                                           </p>
                                           <p>
                                                <span>Bank Name: </span> Habib Metropolitan Bank Limited
                                           </p>	
                                           <p>
                                                <span>Branch:</span> Main Branch, Karachi
                                           </p>	
                                           <p>
                                                <span>IBAN:</span> PK07MPBL0101027140422193
                                           </p>	
                                       </div>
                                   </div>
                                   <div class="col-lg-6">
                                        <div class="secur-tran-detail">
                                            <h3><img src="img/make-gift/zakat.svg" alt=""> For Zakat </h3>
                                            <p>
                                                <span>Title: </span>Habib University Foundation – Zakat Account
                                            </p>
                                            <p>
                                                <span>A/c Number: </span>06-99-64-29313-714-271371
                                            </p>
                                            <p>
                                                <span>Bank Name: </span> Habib Metropolitan Bank Limited
                                            </p>	
                                            <p>
                                                <span>Branch:</span> Star Gate Branch, Karachi
                                            </p>	
                                            <p>
                                                <span>IBAN:</span> PK27MPBL9964287140271371
                                            </p>	
                                        </div>
                                   </div>
                                   <div class="col-lg-12">
                                        <div class="payments-check">
                                            <h3><img src="img/make-gift/cheque.svg" alt="">Payment by Cheque:</h3>
                                            <p>You can mail us a cheque in the name of “Habib University Foundation” at our mailing address or contact the Office of Resource Development for cheque pick up.</p>
                                        </div>

                                        <div class="ofice-resource">
                                            <h3>OFFICE OF RESOURCE DEVELOPMENT</h3>
                                            <p>You can mail us a cheque in the name of “Habib University Foundation” at our mailing address or contact the Office of Resource Development for cheque pick up.</p>
                                            <p> Habib University, Block 18, University Avenue, Off</p>
                                            <p> Sharah-e-Faisal, Gulistan-e-Jauhar, Karachi, Pakistan.</p>
                                            <p> <span>Phone:</span> <a href="tel:+92 21 111 042 242">+92 21 111 042 242</a>  Ext. 4204 | <a href="tel:+92 312 233 8343">+92 312 233 8343</a></p>
                                            <p><span> Email: </span> <a href="mailto:sukaina.bhagat@habib.edu.pk"> sukaina.bhagat@habib.edu.pk</a></p>


                                        </div>
                                        <div class="sec-bottom-con">
                                            <p>Habib University Foundation (HUF) is a non-profit organization registered under Section 42 of the Companies Ordinance, 1984. All donations to HUF are eligible for tax credit under section 61 of the Income Tax Ordinance 2001.</p>
                                            <p> HUF is also Sharia compliant for Zakat collection, certified by Sharia advisor from Jamia Dar ul Uloom, Karachi.</p>
                                        </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <div class="main-accor-title">
                            <span class="flg-acc"><img src="img/make-gift/us.svg" alt="" class="img-fluid"></span>
                            <span class="title-acc">In USA</span>
                        </div>
                        <i class="fas fa-chevron-down"></i>
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                     
                        <div class="secure-content-area">
                            <h3>Make a Secure Gift Online </h3>
                            <p>You can make small regular contribution to Habib and ensure access for the most talented and deserving students to a world class education.  </p>
                            <a href="https://giving.hufus.org/" class="give-now">Give Now</a>
 
                            <div class="secure-pay-details">
                                <div class="row">
                                    <div class="col-xl-6 col-md-12">
                                        <div class="secur-tran-detail">
                                            <h3><img src="img/make-gift/money-transfer.svg" alt=""> Online Transfer: </h3>
                                            <p>
                                                <span>Title: </span>Habib University Foundation US Inc.
                                            </p>
                                            <p>
                                                 <span>A/c Number: </span>101215764
                                            </p>
                                            <p>
                                                 <span>Bank Name: </span> HAB Bank
                                            </p>	
                                            <p>
                                                 <span>Bank Address:</span>99 Madison Ave, New York, N.Y. 10016
                                            </p>	
                                            <p>
                                                 <span>Routing no:</span> 026007362
                                            </p>	
                                            <p>
                                                 <span>Swift code :</span> HANYUS33
                                            </p>	
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                         <div class="payments-check">
                                             <h3><img src="img/make-gift/cheque.svg" alt="">Payment by Cheque:</h3>
                                             <p>In the name of: Habib University Foundation US Inc.</p>
                                         </div>
 
                                         <div class="ofice-resource">
                                             <h3>Letter addressed to: </h3>
                                             <p>Dr. Arvid Nelson, Secretary/Treasurer HUF US Inc.</p>
                                             <p> Greeley Square Station #20150</p>
                                             <p> 4 East 27th Street</p>
                                             <p> New York, New York 10001</p>
                                             <p>In The United States (USD)</p>
 
 
                                         </div>
                                         <div class="sec-bottom-con">
                                             <p>Habib University Foundation U.S. Inc. (H.U.F.U.S.) is a public charity registered under the Internal Revenue Code withtax exempt status under Section 501 (c)(3) of the Code. All contributions to H.U.F.U.S. are deductible under Section 170 of the Code.</p>
                                             <p>Employer number: 45-3218399 | DLN: 104318000</p>
                                             <p>HUF is also Sharia compliant for Zakat collection, certified by Sharia advisor from Jamia Dar ul Uloom, Karachi</p>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <div class="main-accor-title">
                            <span class="flg-acc"><img src="img/make-gift/uk.svg" alt="" class="img-fluid"></span>
                            <span class="title-acc">In UK</span>
                        </div>
                        <i class="fas fa-chevron-down"></i>
                      </button>
                    </h2>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="secure-content-area">
                            <h3>Make a Secure Gift Online  </h3>
                            <p>You can make small regular contribution to Habib and ensure access for the most talented and deserving students to a world class education. </p>
                            <a href="https://giving.habibtrust.org.uk/" class="give-now">Give Now</a>
 
                            <div class="secure-pay-details">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="secur-tran-detail uk-details">
                                            <h3><img src="img/make-gift/money-transfer.svg" alt=""> Online Transfer: </h3>
                                            <h3>For Payments in GBP (Details Below): </h3>
                                            <p>
                                                <span>GBP Correspondent: </span>HBZUGB2L 
                                            </p>
                                            <p>
                                                 <span>A/C with Institution: </span>Habib Bank AG Zurich, West End Branch, London, UK 
                                            </p>
                                            <p>
                                                 <span>Swift code: </span>HBZUGB2L 
                                            </p>	
                                            <p>
                                                 <span>Beneficiary customer:</span> The Habib University Trust 
                                            </p>	
                                            <p>
                                                 <span>IBAN/Account no:</span> GB90HBZU70067214145680 
                                            </p>	
                                            <p>
                                                 <span>Account no:</span> 14145680  
                                            </p>
                                            <p>
                                                 <span>Sort code:</span> 70-06-72  
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                         <div class="secur-tran-detail uk-details">
                                             <span class="space-bet"></span>
                                             <h3> For Payments in USD (Details Below):</h3>
                                             <p>
                                                 <span>USD Correspondent: </span>Bank of New York
                                             </p>
                                             <p>
                                                 <span>A/C with Institution: </span>A/C with Institution 
                                             </p>
                                             <p>
                                                 <span>Swift code: </span>HBZUGB2L 
                                             </p>	
                                             <p>
                                                 <span>Beneficiary customer:</span>The Habib University Trust 
                                             </p>	
                                             <p>
                                                 <span>IBAN/Account no:</span> GB12HBZU60913473145680 
                                             </p>	
                                             <p>
                                                 <span>Account no:</span>14145680 
                                             </p>	
                                             <p>
                                                 <span>Sort code:</span>60-91-34
                                             </p>	
                                         </div>
                                    </div>
                                    <div class="col-lg-12">
                                         <div class="payments-check">
                                             <h3><img src="img/make-gift/cheque.svg" alt="">Payment by Cheque:</h3>
                                             <p>In the name of: The Habib University Trust</p>
                                         </div>
 
                                         <div class="ofice-resource">
                                             <h3>Letter addressed to: </h3>
                                             <p>Syed Kausar Kazmi, Trustee Habib University Trust, Habib House, 
                                                42 Moorgate London, UK EC2R 6JJ k.</p>
                                                <p><span> Email: </span> <a href="mailto:kazmi@habibbank.com"> kazmi@habibbank.com</a></p>
 
 
                                         </div>
                                         <div class="sec-bottom-con">
                                             <p>The Habib University Trust is an approved charity for tax purposes in line with Paragraph 1 of Schedule 6, Finance Act 2010. Charity reference: EW40279</p>
                                             <p>HUF is also Sharia compliant for Zakat collection, certified by Sharia advisor from Jamia Dar ul Uloom, Karachi</p>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
        </div>
    </div>
</section>


<!-- Footer -->
<?php include './include/footer.php' ?>
<!-- Footer -->
