

<!-- Header -->
<?php include './include/header.php' ?>
<!-- Header -->




        
<!-- Main Banner  -->

<div class="main-wraper hu-ambassador">
    
    <!-- Menu -->
    <?php include './include/menu.php' ?>
    <!-- Menu -->

    <div class="student-sup-in">
       <div class="banner-content">
           <h1>Ways Of Giving</h1>
           <h2>Become an HU Reach Ambassador </h2>
       </div>
   </div>

</div>
<!-- Main Banner  -->


<!-- Para Area -->
<section class="create-legacy-para">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="creat-para">
                    <p>Habib University – REACH Program is designed to create a platform where inspirational community members can take the initiative of becoming co-owners of the HU mission. The Ambassadors of the program will advance the mission and support in expansion of Habib University’s community of supporters.</p> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Para Area -->



<!-- Common Scholership area -->
<section class="common-shollership-area">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8 pr-0 p-xs-0">
                <div class="scholer-conternt hu-reach-schol">
                   <!-- <h1>Endowed Program</h1> -->
                   <p> As an ambassador, you will be part of a network, playing a key role in raising awareness of the HU cause within the wider community including your personal network. This is a voluntary role which involves:</p>
                    <ol>
                        <li>Inspiring people to support Higher Education in Pakistan</li>
                        <li>Educating people about Habib University's mission</li>
                        <li>Help Habib University in expanding its community</li>
                    </ol>
                </div>
            </div>
            <div class="col-lg-4 pl-0 p-xs-0">
                <div class="scholer-images">
                    <img src="img/hu-amabsseder/hu-ambassador.webp" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Common Scholership area -->

<!-- Make Gift -->
<?php include './include/make-gift.php' ?>
<!-- Make Gift -->

<!-- Footer -->
<?php include './include/footer.php' ?>
<!-- Footer -->

