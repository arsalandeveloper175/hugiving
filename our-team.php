

<!-- Header -->
<?php include './include/header.php' ?>
<!-- Header -->

        
<!-- Main Banner  -->

<div class="main-wraper contact-page">
       
    <!-- Menu -->
    <?php include './include/menu.php' ?>
    <!-- Menu -->

</div>
<!-- Main Banner  -->


<!-- Contact Content Area -->
<section class="meet-our-team-area">
    <div class="container">
    <h1 class="sec-heading">Our Team</h1>
        <div class="row align-items-center">
            <div class="col-lg-6 pr-0">
                <div class="our-team-content">
                    <div class="team-con-box">
                        <h2>Tatheer Hamdani</h2>
                        <p>President’s Chief of Staff and Director of <br> Global Engagement</p>
                        <a href="mailto:tatheer.hamdani@habib.edu.pk" class="email-team">tatheer.hamdani@habib.edu.pk</a>
                    </div>
                    <div class="team-con-box">
                        <h2>Lubna Ahmed</h2>
                        <p>Director, Resource Development</p>
                        <a href="mailto:lubna.ahmed@habib.edu.pk" class="email-team">lubna.ahmed@habib.edu.pk</a>
                    </div>
                    <div class="team-con-box">
                        <h2>Sukaina Bhagat</h2>
                        <p>Senior Manager, Resource Development</p>
                        <a href="mailto:sukaina.bhagat@habib.edu.pk" class="email-team">sukaina.bhagat@habib.edu.pk</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 pl-0">
                <div class="our-team-form">
                    <h1>Get in touch!</h1>
                    <p>We are here to help and answer any question
                        you might have. We look forward to hearing
                        from you.
                    </p>
                    <div class="get-team-form">
                        <script type="text/javascript" src="https://form.jotform.com/jsform/212551037184046"></script>
                        <!-- <form action="">
                            <div class="form-group">
                                <input type="text" placeholder="Name" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="email" placeholder="Email" class="form-control">
                            </div>
                            <div class="form-group slec-item">
                               <select name="" id="">
                                   <option value="" selected disabled>Subject</option>
                                   <option value="" >Subject 1</option>
                                   <option value="" >Subject 2</option>
                                   <option value="" >Subject 3</option>
                               </select>
                                   <i class="fas fa-chevron-down"></i>
                            </div>
                            <div class="form-group">
                                <textarea placeholder="Message" name="" id="" cols="30" rows="10"></textarea>
                            </div>
                            <div class="form-group mb-0 btn-set">
                                <button class="send-btn">Send</button>
                            </div>
                        </form> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Content Area -->

<!-- Footer -->
<?php include './include/footer.php' ?>
<!-- Footer -->

  