

<footer class="main-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5">
                <div class="footer-right">
                    <div class="foot-logo">
                        <a href="https://habib.edu.pk/" target="_blank"> 
                        <img src="img/footer-logo.svg" alt="" class="footer-logo img-fluid">
                        </a>
                        <!-- <div class="top-span">
                            <h4>Habib University</h4>
                            <h4>Shaping features</h4>
                        </div> -->
                    </div>
                    <div class="social-links">
                        <h4>Follow us</h4>
                        <ul class="socail-list">
                            <li><a target="_blank" href="https://www.facebook.com/HabibUniversity"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a target="_blank" href="https://www.linkedin.com/school/habib-university/"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/habibuniversity/"><i class="fab fa-instagram"></i></a></li>
                            <li><a target="_blank" href="https://twitter.com/habibuniversity"><i class="fab fa-twitter"></i></a></li>
                            <li><a target="_blank" href="https://www.youtube.com/user/HabibUni"><i class="fab fa-youtube"></i></a></li>
                        </ul>
                    </div>
                    <p class="copy-right">© Habib University - All Rights Reserved | <a href="https://habib.edu.pk/privacy-statement/" target="_blank"> Privacy Statement</a></p>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="footer-contact-det">
                    <div class="det-box">
                        <h4>Email</h4>
                        <p> <a href="mailto:giving@habib.edu.pk"> giving@habib.edu.pk</a></p>
                    </div>
                    <div class="det-box">
                        <h4>Call us at</h4>
                        <p><a href="tel:giving@habib.edu.pk"> +92 21 111 042 242 (Ext. 4444)</a> <span>|</span> <a href="tel:+923122338343">+92 312 2338343</a></p>
                    </div>
                    <div class="det-box">
                        <h4>Mailing Address</h4>
                        <p>HUF, Block 18, Gulistan-e-Jauhar - University Avenue, Off Shahrah-e-Faisal, <br> Karachi - 75290, Sindh, Pakistan</p>
                    </div>
                    <p class="copy-right">© Habib University - All Rights Reserved | <a href="https://habib.edu.pk/privacy-statement/" target="_blank"> Privacy Statement</a></p>
                </div>
                
            </div>
        </div>
    </div>
</footer>


    <script src="js/plugin.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>