<div class="modal fade modal-box-news" id="newsone" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-inner-box">
          <button type="button" class="close" data-dismiss="modal"><img src="img/close-icon.svg" alt=""></button>
       
                <p>Habib University hosted the Mashooqullah family on April 9, 2021 to dedicate the Khawaja Mashooqullah Music Room, a creative space that allows students to learn music from leading faculty, and reclaims the musical heritage of the South Asian subcontinent for the next generation. The collaboration between Habib University and the Mashooqullah family highlights the university’s commitment to connecting the students to their culture and roots, and revives instruction of a discipline that has long been neglected in higher education in Pakistan.</p>
      
                <a target="_blank" href="https://habib.edu.pk/HU-news/khawaja-mashooqullah-music-room-celebrating-the-splendor-of-south-asian-music/" class="read-more-modal">
                  <span> Read Complete Story</span>
                 <img src="img/read-more.svg" alt="">
                </a>
            
        </div>
        
      </div>
      
    </div>
  </div>



  <div class="modal fade modal-box-news" id="newstwo" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-inner-box">
          <button type="button" class="close" data-dismiss="modal"><img src="img/close-icon.svg" alt=""></button>
       
                <p>
    Mr. Mohsin Nathani, the President of HabibMetro Bank and a strong supporter of Habib University, visited Habib University campus with his family on May 6, 2021. The visit marked a personal contribution Mr. Nathani decided to make to Habib University, supporting the provision of accessible higher education to the talented and promising youth of Pakistan.
    </p>
      
                <a target="_blank" href="https://habib.edu.pk/HU-news/ceo-of-habibmetro-bank-applauds-habibs-need-blind-admission-policy/" class="read-more-modal">
                  <span>Read Complete Story </span>
                  <img src="img/read-more.svg" alt="">
                </a>
            
        </div>
        
      </div>
      
    </div>
  </div>



  <div class="modal fade modal-box-news" id="newsthree" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-inner-box">
          <button type="button" class="close" data-dismiss="modal"><img src="img/close-icon.svg" alt=""></button>
       
                <p>To address the challenges prevalent in Pakistani higher education and to discuss the role of wider community ownership in making it accessible to all in Pakistan, Wasif Rizvi, President Habib University, was invited to be the keynote speaker at an interactive webinar, ‘Mobilizing Community for Higher Education,’ organized by the Corporate Pakistan Group on June 19, 2021. Adnan Rizvi, the Head of Advisory at KPMG and a Managing Committee member of Corporate Pakistan Group, moderated the session.</p>
      
                <a target="_blank" href="https://habib.edu.pk/HU-news/community-ownership-essential-for-improvement-of-higher-education/" class="read-more-modal">
                  <span> Read Complete Story  </span>
                  <img src="img/read-more.svg" alt="">
                </a>
            
        </div>
        
      </div>
      
    </div>
  </div>