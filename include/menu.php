   <!-- SideNav -->
   <div id="mySidenav" class="sidenav">
        <div class="side-menu-inner">
            <div class="side-nav-header">
                <a href="https://habib.edu.pk/" class="side-nav-logo"><img src="img/side-nav-logo.svg" alt=""></a>
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><img src="img/sid-nav-arrow.svg" alt=""></a>
            </div>
            <div class="sid-menu-links">
               <ul>
                <li> <a href="about">About</a></li>
                <li><a href="impact">Impact</a></li>
                <li> <a href="our-community">Our Community</a></li>
                <li class="drops-menu"> <a href="javascript:;" data-toggle="collapse" data-target="#waysofgiving">Ways of Giving <i class="fas fa-chevron-down"></i></a>
                    <div id="waysofgiving" class="collapse">
                        <ul class="sub-menu">
                            <li><a href="student-support">Student Support</a></li>
                            <li><a href="create-legacy">Create your own legacy</a></li>
                            <li><a href="intellectual-resources">Intellectual Resources</a></li>
                            <li><a href="hu-reach">Become an HU reach Ambassador</a></li>
                         </ul>
                      </div>
                </li>
                <li class="drops-menu"> <a href="javascript:;" data-toggle="collapse" data-target="#chapters">Chapters <i class="fas fa-chevron-down"></i></a>
                    <div id="chapters" class="collapse">
                        <ul class="sub-menu">
                            <li> <a href="https://hufus.org/" target="_blank"> <img src="img/make-gift/us.svg" alt="">  HUFUS Inc</a></li>
                            <li> <a href="https://habibtrust.org.uk/" target="_blank"><img src="img/make-gift/uk.svg" alt="">  HU Trust UK </a></li>
                         </ul>
                      </div>
                </li>
                    <li> <a href="our-team">Our Team</a></li>
                </ul>
            </div>
            <div class="sid-menu-givesnow"> 
                <h2>Give Now</h2>
                <div class="flag-item">
                    <a href="https://giving.habib.edu.pk/" target="_blank"><img src="img/make-gift/pk.svg" alt=""> Pakistan</a>
                    <a href="https://giving.hufus.org/" target="_blank"><img src="img/make-gift/us.svg" alt=""> US</a>
                    <a href="https://giving.habibtrust.org.uk/" target="_blank"><img src="img/make-gift/uk.svg" alt=""> UK</a>
                    <a href="make-a-gift"><img src="img/more-option.svg" alt=""> More Options</a>
                </div>
            </div>
        </div>
      </div>
    <!-- SideNav -->

    <!-- header -->
    <header class="main-head">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-9 col-sm-12 col-12">
                    <div class="main-nav">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <div class="navbar-brand" href="javascript:;"> 
                                  <a href="https://habib.edu.pk/">
                                    <!-- <img src="img/white-logo.svg" alt="" class="img-fluid respon-logo"></a> -->
                                  <a href="index">
                                    <span class="giving-title">Giving</span>
                                   </a>
                                </div>
                            <button onclick="openNav()" class="navbar-toggler nav-main" type="button" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                              <!-- <span class="navbar-toggler-icon"></span> -->
                              <img src="img/menu-icon.svg" class="bar-noscroll" alt="">
                            <img src="img/menu-icon-colored.svg" alt="" class="bar-scroll">
                            </button>
                                <div class="collapse navbar-collapse" id="navbarNav">
                                  <ul class="navbar-nav ml-auto center-main-menu">
                                    <li class="nav-item active">
                                      <a class="nav-link" href="about">About
                                          <span class="hvr-anim"></span>
                                      </a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" href="impact">Impact
                                        <span class="hvr-anim"></span>
                                    </a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" href="our-community">Our Community</a>
                                    </li>
                                    <li class="nav-item  dropdown dropdown-6">
                                      <a class="nav-link" href="javascript:;" id="wayDropDown" role="button" data-toggle="dropdown">Ways Of Giving <i class="fas fa-chevron-down"></i></a>
                                      <div class="dropdown-menu  dropdown_menu--animated dropdown_menu-6" aria-labelledby="wayDropDown">
                                        <a class="dropdown-item" href="student-support">Student Support</a>
                                        <a class="dropdown-item" href="create-legacy">Create your own legacy</a>
                                        <a class="dropdown-item" href="intellectual-resources">Intellectual Resources</a>
                                        <a class="dropdown-item" href="hu-reach">Become an HU reach <br> Ambassador</a>
                                      </div>
                                    </li>
                                    
                                    <li class="nav-item  dropdown dropdown-6">
                                      <a class="nav-link" href="javascript:;" id="wayDropDown" role="button" data-toggle="dropdown">Chapters <i class="fas fa-chevron-down"></i></a>
                                      <div class="dropdown-menu  dropdown_menu--animated dropdown_menu-6" aria-labelledby="wayDropDown">
                                        <a class="dropdown-item" href="https://hufus.org/" target="_blank"><img src="img/make-gift/us.svg" alt=""> HUFUS Inc</a>
                                        <a class="dropdown-item" href="https://habibtrust.org.uk/" target="_blank"><img src="img/make-gift/uk.svg" alt=""> HU Trust UK</a>
                                      </div>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" href="our-team">Our Team</a>
                                    </li>
                                  </ul>
                                </div>
                          </nav>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-3 col-12">
                    <div class="flag-menu-nav">
                        <div class="dropdown">
                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Give Now <i class="fas fa-chevron-down"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item" href="https://giving.habib.edu.pk/" target="_blank"><span class="flg-item">In Pakistan <img src="img/make-gift/pk.svg" alt=""></span> </a>
                              <a class="dropdown-item" href="https://giving.hufus.org/" target="_blank"><span class="flg-item">In USA <img src="img/make-gift/us.svg" alt=""></span></a>
                              <a class="dropdown-item" href="https://giving.habibtrust.org.uk/" target="_blank"><span class="flg-item">In UK <img src="img/make-gift/uk.svg" alt=""></span></a>
                              <a class="dropdown-item" href="make-a-gift" target="_blank"><span class="flg-item">More Options<img src="img/more-option.svg" alt=""></span></a>
                            </div>
                          </div>
                    </div>
                </div>
                <div class="col-lg-1">
                    <div class="white-logo-hb">
                        <a href="https://habib.edu.pk/" target="_blank"><img src="img/white-logo.svg" alt="" class="img-fluid"></a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header -->